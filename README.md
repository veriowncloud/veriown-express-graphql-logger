# @veriown/express-graphql-logger

> Express Logging middleware for Graphql Requests

It logs requests in Apache Combined format.

## Installation

```
npm install --save @veriown/express-graphql-logger
```

## Usage

```
const express = require('express');
const graphqlLogger = require('@veriown/express-graphql-logger');

const app = express();

const loggerInstance = { info: fn }; // any object which has `info` function e.g a winston instance

app.use(graphqlLogger(loggerInstance, { splitLog: false })); // splitLog: Log request and response separately. Default: false. Log on response only.
```
