const jwt = require('jsonwebtoken');
const clfDate = require('clf-date');
const gql = require('graphql-tag');

const responseBodyJson = (body) => {
  const stringBody = body && body.toString();

  try {
    return JSON.parse(stringBody);
  } catch (err) {
    return {};
  }
};

const getAuthenticatedClient = (req, logger) => {
  const accessToken = req.headers.authorization && req.headers.authorization.replace('Bearer ', '');
  if (!accessToken) return null;

  let decodedAccessToken = '';
  try {
    decodedAccessToken = jwt.decode(accessToken);
  } catch (err) {
    logger.info('Failed to decode JWT token for logger', err);
  }

  if (!decodedAccessToken) return null;

  return Object.keys(decodedAccessToken)
    .filter((key) => key !== 'iat' && key !== 'exp')
    .map((identity) => {
      return {
        type: identity,
        id: decodedAccessToken[identity].id
      };
    });
};

const getIpAddress = (req) => {
  if (req.headers['x-forwarded-for']) {
    return req.headers['x-forwarded-for'].split(',')[0].split(':')[0];
  }

  return req.connection.remoteAddress || req.ip;
};

/* eslint-disable no-useless-escape */
const getApacheCombinedMessage = (req, res) => `${getIpAddress(req)} - - [${clfDate(new Date())}] \"${req.method} ${req.originalUrl} HTTP/${
  req.httpVersion
  }\" ${res ? res.statusCode : '-'} ${req.headers.referer || '-'} ${req.headers['user-agent']}`;

const getGraphqlOperation = (query) => {
  if (!query) {
    return '';
  }

  const ast = gql(query);
  const operationDefinition = ast.definitions.find((d) => d.kind === 'OperationDefinition');

  const operationType = operationDefinition.operation;
  const operationName = operationDefinition.name ? operationDefinition.name.value : '';
  const selections = operationDefinition.selectionSet.selections
    .map((s) => s.name.value)
    .join(', ');

  return `${operationType} ${operationName || ''} (${selections})`;
};

const getQueryValues = (key, payload) => {
  if (!payload) {
    return null;
  }
  const ast = gql(payload);
  const operationDefinition = ast.definitions.find((d) => d.kind === 'OperationDefinition');
  const selectionArguments = operationDefinition.selectionSet.selections.map((d) => d.arguments);
  const flatArray = [].concat(...selectionArguments);
  const selectedArg = flatArray.find((arg) => arg.name.value === key);

  return selectedArg ? selectedArg.value.value : null;
};

const getGraphqlLogger = (logger, req, resBody) => {
  const graphqlPayload = req.method === 'GET' ? { ...req.query } : { ...req.body };

  const isGraphqlRequest = graphqlPayload && (graphqlPayload.query || graphqlPayload.mutation);
  let graphqlOperation = '';

  try {
    // parsing may fail if the graphql query is invalid
    graphqlOperation = getGraphqlOperation(graphqlPayload.query);
  } catch (err) {
    // pass
  }

  if (graphqlPayload && graphqlPayload.query) {
    const refreshTokenValue = getQueryValues('refreshToken', graphqlPayload.query);
    graphqlPayload.query = graphqlPayload.query.replace(refreshTokenValue, '[REDACTED]');
    const accessTokenValue = getQueryValues('accessToken', graphqlPayload.query);
    graphqlPayload.query = graphqlPayload.query.replace(accessTokenValue, '[REDACTED]');
  }

  let confidentialVariables = {};
  if (
    graphqlPayload.variables
    && (graphqlPayload.variables.refreshToken || graphqlPayload.variables.accessToken)
  ) {
    if (graphqlPayload.variables.refreshToken) {
      confidentialVariables.refreshToken = '[REDACTED]';
    }
    if (graphqlPayload.variables.accessToken) {
      confidentialVariables.accessToken = '[REDACTED]';
    }
  } else {
    confidentialVariables = graphqlPayload.variables;
  }

  const updatedpayload = {
    ...graphqlPayload,
    variables: confidentialVariables
  };

  return logger.child({
    graphqlPayload: isGraphqlRequest && updatedpayload,
    graphqlOperation,
    errors: resBody && resBody.errors
  });
};

module.exports = (winstonInstance, { splitLog = false } = {}) => (req, res, next) => {
  const { end } = res;
  const requestId = req.id || req.requestId || req.headers['x-request-id'];

  let logger = winstonInstance.child({
    requestId,
    ip: getIpAddress(req),
    '@timestamp': new Date(),
    userAgent: req.headers['user-agent']
  });

  const identities = getAuthenticatedClient(req, logger);

  logger = logger.child({
    identity: identities && identities[0]
  });

  if (splitLog) {
    const message = getApacheCombinedMessage(req);
    logger = getGraphqlLogger(logger, req);
    logger.verbose(message, { type: 'request' });
  }

  res.end = function endResponse(chunk, encoding) {
    res.end = end;
    res.end(chunk, encoding);

    const resBody = responseBodyJson(chunk);
    const message = getApacheCombinedMessage(req, res);
    logger = getGraphqlLogger(logger, req, resBody);

    logger.verbose(message, { type: 'response' });
  };

  return next();
};
